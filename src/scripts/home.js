$(document).ready(function(){
    $(document).attr('nb', 1);
    $("#test").html("Bonjour !");
    $("#clickme").click(clickme_click);
    $("#enregistrer").click(enregistrer_click);
    $("#nb").change(nb_change);
    appel_ajax();
});

function nb_change(e) {
    console.log(e);
    var _old = $(document).attr('nb');
    var _new = $("#nb").val();
    console.log(_old);
    console.log(_new);
    if (_old < _new) {
        // ajoute 1
        $("#zone").append(
            '<div id="nb' + _new + '" class="form-group row">'
            + '<label for="debut' + _new + '" class="col-2">début ' + _new + ' :</label>'
            + '<input id="debut' + _new + '" type="date" class="form-control col-4">'
            + '<label for="fin' + _new + '" class="col-2">fin ' + _new + ' :</label>'
            + '<input id="fin' + _new + '" type="date" class="form-control col-4">'
            + '</div>'
        );
    } else {
        // enlève 1
        $("#nb" + _old).remove();
    }
    $(document).attr('nb', _new);
}

function clickme_click() {
    alert('You click me !');
}

function appel_ajax() {
    $.ajax({
        url: './src/api/get-list.php',
        success: function(data) {
            console.log(data);
            var $array = JSON.parse(data);
            $.each($array, function(i, row) {
                $("#list").append($('<option>', {value: row.id, text: row.nom}));
            });
        }
    });
}

function enregistrer_click() {
    
    var nom = $("#nom").val();
    var prenom = $("#prenom").val();

    data = new FormData();
    data.append("nom", nom);
    data.append("prenom", prenom);

    $.ajax({
        url: './src/api/enregistrer.php',
        method: 'POST',
        data: data,
        processData: false,  // indique à jQuery de ne pas traiter les données
        contentType: false,   // indique à jQuery de ne pas configurer le contentType
        success: function(data) {
            console.log(data);
        }
    });
}
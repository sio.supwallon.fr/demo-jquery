<?php
namespace Demo\dao;

use Demo\model\Personne;
use Demo\view\View;
use PDO;

class PersonneDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * FROM `personne` WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            View::setTemplate('error-db-statement');
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Personne::class);

        $personne = $sth->fetch();

        return $personne;
    }
}
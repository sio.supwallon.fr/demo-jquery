<?php
namespace Demo\dao;

use Demo\view\View;
use PDO;
use PDOException;

class Database
{
    private static $_driver   = "mysql";
    private static $_host     = "host=127.0.0.1;";
    private static $_port     = "port=3308;";
    private static $_charset  = "charset=utf8;";
    private static $_database = "dbname=demo;";
    private static $_user     = "demo";
    private static $_password = "demo";

    private static $_dsn;
    private static $_connection;
    
    public static function get_connection()
    {
        if(!isset(self::$_connection)) {
            self::$_dsn = self::$_driver . ":" 
                        . self::$_host 
                        . self::$_port 
                        . self::$_charset 
                        . self::$_database;
            try {
                self::$_connection = new PDO(self::$_dsn, self::$_user, self::$_password);
            } catch (PDOException $e) {
                View::setTemplate('error-db-connection');
                View::display();
                die();
            }
        }
        return self::$_connection;
    }
}
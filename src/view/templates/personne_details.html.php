<h1>Détails de la personne</h1>
<div class="row">
    <label for="nom" class="col-1">Nom:</label> 
    <div id="nom" class="col-2"><?= $personne->nom; ?></div>
</div>
<div class="row">
    <label for="prenom" class="col-1">Prénom:</label> 
    <div id="prenom" class="col-2"><?= $personne->prenom; ?></div>
</div>
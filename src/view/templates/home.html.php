<h1>Home</h1>
<p>Welcome to the home page.</p>
<div id="test"></div>
<div class="text-center">
    <button id="clickme" class="btn btn-outline-primary">Click Me</button>
</div>
<div class="row">
    <div class="offset-5 col-2 border text-center mt-3">
        <select id="list" class="form-control"></select>
    </div>
</div>
<form>
    <div class="row form-group">
        <label for="nom" class="col-form-label col-1">Nom:</label>
        <div class="col-2">
            <input id="nom" name="nom" class="form-control">
        </div>
    </div>
    <div class="row form-group">
        <label for="prenom" class="col-form-label col-1">Prénom:</label>
        <div class="col-2">
            <input id="prenom" name="prenom" class="form-control">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-3 text-right">
            <button type="button" id="enregistrer" class="btn btn-outline-primary">Enregistrer</button>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-1">
        <input id="nb" type="number" value="1" min="1" max="3" class="form-control">
    </div>
    <div id="zone" class="col-11 border">
        <div class="form-group row">
            <label for="debut1" class="col-2">début 1 :</label>
            <input id="debut1" type="date" class="form-control col-4">
            <label for="fin1" class="col-2">fin 1 :</label>
            <input id="fin1" type="date" class="form-control col-4">
        </div>
    </div>
</div>

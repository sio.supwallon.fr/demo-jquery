<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Exemple JQuery</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    </head>
    <body>
<?php require 'navbar.html.php'; ?>
        <main class="container">
<?php require $template; ?>
        </main>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="vendor/components/jquery/jquery.min.js"></script>
        <script src="vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<?php if(file_exists("src/scripts/" . $script)): ?>        
        <script src="src/scripts/<?= $script ?>"></script>
<?php endif; ?>        
    </body>
</html>
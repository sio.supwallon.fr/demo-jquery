<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Demo JQuery</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item<?= ($controller == 'home')?" active":""; ?>">
        <a class="nav-link" href="?controller=home&action=default">Home</a>
      </li>
      <li class="nav-item<?= ($controller == 'personne' && $action == 'details' && isset($personne) && $personne->id == 1)?" active":""; ?>">
        <a class="nav-link" href="?controller=personne&action=details&id=1">Personne 1</a>
      </li>
      <li class="nav-item<?= ($controller == 'personne' && $action == 'details' && isset($personne) && $personne->id == 2)?" active":""; ?>">
        <a class="nav-link" href="?controller=personne&action=details&id=2">Personne 2</a>
      </li>
      <li class="nav-item<?= ($controller == 'personne' && $action == 'bidouille')?" active":""; ?>">
        <a class="nav-link" href="?controller=personne&action=bidouille">Bidouille</a>
      </li>
    </ul>
  </div>
</nav>
<?php
namespace Demo;

use Demo\controllers\HomeController;
use Demo\controllers\PersonneController;

$controller = filter_input(INPUT_GET, 'controller', FILTER_SANITIZE_STRING);

switch($controller)
{
    case 'home':
        HomeController::route();
    break;
    case 'personne':
        PersonneController::route();
    break;
    default:
        HomeController::route();
    break;
}
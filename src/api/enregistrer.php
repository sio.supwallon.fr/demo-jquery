<?php
if (isset($_POST['nom']) && isset($_POST['prenom'])) {
    $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING);
    $prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_STRING);
    echo json_encode(['result' => 'ok', 'nom' => $nom, 'prenom' => $prenom]);
} else
{
    echo json_encode(['result' => 'error']);
}
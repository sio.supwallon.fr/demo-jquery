<?php
namespace Demo\controllers;

use Demo\view\View;

class HomeController
{
    public static function route()
    {
        $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

        switch($action)
        {
            case 'default':
                self::default_action();
            break;
            default:
                self::default_action();
            break;
        }
    }

    public static function default_action()
    {
        View::setTemplate('home');
        View::display();
    }
}
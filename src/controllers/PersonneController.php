<?php
namespace Demo\controllers;

use Demo\model\Personne;
use Demo\view\View;

class PersonneController
{
    public static function route()
    {
        $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

        switch($action)
        {
            case 'details':
                $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                self::details_action($id);
            break;
            case 'bidouille':
                self::bidouille_action();
            break;
        }
    }

    public static function details_action($id)
    {
        $personne = Personne::get($id);

        View::setTemplate('personne_details');
        View::bindParam("personne", $personne);
        View::display();
    }

    public static function bidouille_action()
    {
        $var = new Personne();

        $var->id = 15;
        $var->nom = 'DESCHAMPS';
        $var->prenom = 'Benoit';

        $var2 = $var->id;

        View::setTemplate('bidouille');
        View::bindParam("var", $var);
        View::display();
    }
}
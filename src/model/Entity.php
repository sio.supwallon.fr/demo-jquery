<?php
namespace Demo\model;

use Exception;

abstract class Entity
{
    public function __set($name, $value)
    {
        //echo "$name = $value\r\n";
        $class = get_class($this);
        $method = "set_" . $name;
        if(! property_exists($this, $name)) {
            throw new Exception("Property $name does not exist in $class.");
        }
        if (! method_exists($this, $method)) {
            throw new Exception("Property $name is not writable in $class.");
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        //echo "$name\r\n";
        $class = get_class($this);
        $method = "get_" . $name;
        if(! property_exists($this, $name)) {
            throw new Exception("Property $name does not exist in $class.");
        }
        if (! method_exists($this, $method)) {
            throw new Exception("Property $name is not readable in $class.");
        }
        return $this->$method();
    }
}
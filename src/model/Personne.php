<?php
namespace Demo\model;
use Demo\dao\PersonneDao;

class Personne extends Entity
{
    protected $id;
    protected function get_id() { return $this->id; }
    protected function set_id($value) { $this->id = $value; }

    protected $nom;
    protected function get_nom() { return $this->nom; }
    protected function set_nom($value) { $this->nom = $value; }

    public $prenom;

    public static function get($id) { return PersonneDao::get($id); }
}